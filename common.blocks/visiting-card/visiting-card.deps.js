({
    shouldDeps : [
        {
            elems : [
                'inner', 'name', 'post', 'contacts', 'contacts-name', 'contact',
                { elem : 'image', mods : { default : 'yes' } }
            ]
        },
        'email', 'phone-number'
    ]
})
