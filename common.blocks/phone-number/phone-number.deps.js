({
    shouldDeps : [
        {
            elems : [
                'text',
                {
                    elem : 'logo',
                    mods : {
                        type : ['life', 'mts', 'kyivstar']
                    }
                }
            ]
        },
        'link'
    ]
})
