/** @class declension */
modules.define('declension', [], function(provide) {
    // TODO: автоматическое склонение
    var vocabulary = {
        alupka : {
            partOfSpeech : 'noun',
            nominative : { singular : 'Алупка' },
            genitive : { singular : 'Алупки' },
            dative : { singular : 'Алупке' },
            accusative : { singular : 'Алупку' },
            instrumental : { singular : 'Алупкой' },
            prepositional : { singular : 'Алупке' },
            locative : { singular : 'Алупке' }
        },
        alushta : {
            partOfSpeech : 'noun',
            nominative : { singular : 'Алушта' },
            genitive : { singular : 'Алушты' },
            dative : { singular : 'Алуште' },
            accusative : { singular : 'Алушту' },
            instrumental : { singular : 'Алуштой' },
            prepositional : { singular : 'Алуште' },
            locative : { singular : 'Алуште' }
        },
        simferopol : {
            partOfSpeech : 'noun',
            nominative : { singular : 'Симферополь' },
            genitive : { singular : 'Симферополя' },
            dative : { singular : 'Симферополю' },
            accusative : { singular : 'Симферополь' },
            instrumental : { singular : 'Симферополем' },
            prepositional : { singular : 'Симферополе' },
            locative : { singular : 'Симферополе' }
        },
        yalta : {
            partOfSpeech : 'noun',
            nominative : { singular : 'Ялта' },
            genitive : { singular : 'Ялты' },
            dative : { singular : 'Ялте' },
            accusative : { singular : 'Ялту' },
            instrumental : { singular : 'Ялтой' },
            prepositional : { singular : 'Ялте' },
            locative : { singular : 'Ялте' }
        },
        crimea : {
            partOfSpeech : 'noun',
            nominative : { singular : 'Крым' },
            genitive : { singular : 'Крыма' },
            dative : { singular : 'Крыму' },
            accusative : { singular : 'Крым' },
            instrumental : { singular : 'Крымом' },
            prepositional : { singular : 'Крыме' },
            locative : { singular : 'Крыму' }
        },
        person : {
            partOfSpeech : 'noun',
            nominative : { singular : 'человек', plural : 'люди' },
            genitive : { singular : 'человека', plural : 'людей' },
            dative : { singular : 'человеку', plural : 'людям' },
            accusative : { singular : 'человека', plural : 'людей' },
            instrumental : { singular : 'человеком', plural : 'людьми' },
            prepositional : { singular : 'человеке', plural : 'людях' },
            locative : { singular : 'человеке', plural : 'людях' }
        },
        adult : {
            partOfSpeech : 'adjective',
            gender : 'masculine',
            nominative : { singular : 'взрослый', plural : 'взрослые' },
            genitive : { singular : 'взрослого', plural : 'взрослых' },
            dative : { singular : 'взрослому', plural : 'взрослым' },
            accusative : { singular : 'взрослого', plural : 'взрослых' },
            instrumental : { singular : 'взрослым', plural : 'взрослыми' },
            prepositional : { singular : 'взрослом', plural : 'взрослых' },
            locative : { singular : 'взрослом', plural : 'взрослых' }
        },
        child : {
            partOfSpeech : 'noun',
            nominative : { singular : 'ребенок', plural : 'дети' },
            genitive : { singular : 'ребенка', plural : 'детей' },
            dative : { singular : 'ребенку', plural : 'детям' },
            accusative : { singular : 'ребенка', plural : 'детей' },
            instrumental : { singular : 'ребенком', plural : 'детьми' },
            prepositional : { singular : 'ребенке', plural : 'детях' },
            locative : { singular : 'ребенке', plural : 'детях' }
        },
        additionalPlace : {
            partOfSpeech : 'noun',
            nominative : { singular : 'доп. место', plural : 'доп. места' },
            genitive : { singular : 'доп. места', plural : 'доп. мест' },
            dative : { singular : 'доп. месту', plural : 'доп. местам' },
            accusative : { singular : 'доп. место', plural : 'доп. места' },
            instrumental : { singular : 'доп. местом', plural : 'доп. местами' },
            prepositional : { singular : 'доп. месте', plural : 'доп. местах' },
            locative : { singular : 'доп. месте', plural : 'доп. местах' }
        },
        second : {
            partOfSpeech : 'noun',
            nominative : { singular : 'секунда', plural : 'секунды' },
            genitive : { singular : 'секунды', plural : 'секунд' },
            dative : { singular : 'секунде', plural : 'секундам' },
            accusative : { singular : 'секунду', plural : 'секунды' },
            instrumental : { singular : 'секундой', plural : 'секундами' },
            prepositional : { singular : 'секунде', plural : 'секундах' },
            locative : { singular : 'секунде', plural : 'секундах' }
        },
        minute : {
            partOfSpeech : 'noun',
            nominative : { singular : 'минута', plural : 'минуты' },
            genitive : { singular : 'минуты', plural : 'минут' },
            dative : { singular : 'минуте', plural : 'минутам' },
            accusative : { singular : 'минуту', plural : 'минуты' },
            instrumental : { singular : 'минутой', plural : 'минутами' },
            prepositional : { singular : 'минуте', plural : 'минутах' },
            locative : { singular : 'минуте', plural : 'минутах' }
        },
        hour : {
            partOfSpeech : 'noun',
            nominative : { singular : 'час', plural : 'часы' },
            genitive : { singular : 'часа', plural : 'часов' },
            dative : { singular : 'часу', plural : 'часам' },
            accusative : { singular : 'час', plural : 'часы' },
            instrumental : { singular : 'часом', plural : 'часами' },
            prepositional : { singular : 'часе', plural : 'часах' },
            locative : { singular : 'часе', plural : 'часах' }
        },
        day : {
            partOfSpeech : 'noun',
            nominative : { singular : 'день', plural : 'дни' },
            genitive : { singular : 'дня', plural : 'дней' },
            dative : { singular : 'дню', plural : 'дням' },
            accusative : { singular : 'день', plural : 'дни' },
            instrumental : { singular : 'днем', plural : 'днями' },
            prepositional : { singular : 'дне', plural : 'днях' },
            locative : { singular : 'дне', plural : 'днях' }
        },
        week : {
            partOfSpeech : 'noun',
            nominative : { singular : 'неделя', plural : 'недели' },
            genitive : { singular : 'недели', plural : 'недель' },
            dative : { singular : 'неделе', plural : 'неделям' },
            accusative : { singular : 'неделю', plural : 'недели' },
            instrumental : { singular : 'неделей', plural : 'неделями' },
            prepositional : { singular : 'неделе', plural : 'неделях' },
            locative : { singular : 'неделе', plural : 'неделях' }
        },
        month : {
            partOfSpeech : 'noun',
            nominative : { singular : 'месяц', plural : 'месяцы' },
            genitive : { singular : 'месяца', plural : 'месяцев' },
            dative : { singular : 'месяцу', plural : 'месяцам' },
            accusative : { singular : 'месяц', plural : 'месяцы' },
            instrumental : { singular : 'месяцем', plural : 'месяцами' },
            prepositional : { singular : 'месяце', plural : 'месяцах' },
            locative : { singular : 'месяце', plural : 'месяцах' }
        },
        halfyear : {
            partOfSpeech : 'noun',
            nominative : { singular : 'полугодие', plural : 'полугодия' },
            genitive : { singular : 'полугодия', plural : 'полугодиев' },
            dative : { singular : 'полугодию', plural : 'полугодиям' },
            accusative : { singular : 'полугодие', plural : 'полугодия' },
            instrumental : { singular : 'полугодием', plural : 'полугодиями' },
            prepositional : { singular : 'полугодии', plural : 'полугодиях' },
            locative : { singular : 'полугодии', plural : 'полугодиях' }
        },
        year : {
            partOfSpeech : 'noun',
            nominative : { singular : 'год', plural : 'годы' },
            genitive : { singular : 'года', plural : 'годов' },
            dative : { singular : 'году', plural : 'годам' },
            accusative : { singular : 'год', plural : 'годы' },
            instrumental : { singular : 'годом', plural : 'годами' },
            prepositional : { singular : 'годе', plural : 'годах' },
            locative : { singular : 'годе', plural : 'годах' }
        },

        'владелец/контактное лицо' : { accusative : { singular : 'владельца/контактное лицо' } },
        'ссылка (uri)' : { accusative : { singular : 'ссылку (uri)' } },
        'точка на карте' : { accusative : { singular : 'точку на карте' } },
        цена : { accusative : { singular : 'цену' } }
    };

    /**
     * Склоняет переданное слово
     * @param {Number} [num] Кол-во, если нужно склонять учитывая числительное
     * @param {String} word Слово(ключ в словаре) для склонения
     * @param {String} caseName Названия целевого падежа
     */
    provide(function(num, word, caseName) {
        var number = isFinite(+num) ? +num : 1,
            isFraction = number % 1 !== 0,
            significantFigures = Number(String(number).split('.')[isFraction ? 1 : 0].substr(-2)),
            significantLastFigure = Number(String(significantFigures).substr(-1));

        if(!vocabulary[word]) return word;
        if(isFraction) return vocabulary[word].accusative.plural;
        // Числительное один (одна, одно) согласуется с существительным в роде, числе и падеже
        // (ср.: один день, одним днем, в одну неделю и т.д.).
        if((number > 20 ? significantLastFigure : number) === 1) return vocabulary[word][caseName].singular;
        // Числительные два, три, четыре в форме И.п. (и В. п. при неодушевленных существительных)
        // управляют формой Р.п. единственного числа существительных: два дня, два товарища, три окна.
        // В остальных падежах эти числительные согласуются с существительными во множественном числе.

        if([2, 3, 4].indexOf(number > 20 ? significantLastFigure : number) > -1) {
            // Субстантивированные прилагательные мужского и среднего рода в сочетании с числительными два, три, четыре
            // употребляются в форме Р.п. мн.ч. (ср.: два дежурных, три насекомых), а субстантивированные прилагательные
            // женского рода - в форме Р. или И.-В. п. множественного числа (ср.: две запятых - две запятые и т.п.).
            if(vocabulary[word].partOfSpeech === 'adjective') {
                if(caseName === 'nominative' && ['masculine', 'neuter'].indexOf(vocabulary[word].gender) > -1) {
                    return vocabulary[word].genitive.plural;
                } else {
                    return vocabulary[word][caseName].plural;
                }
            } else if(['nominative', 'accusative'].indexOf(caseName) > -1) {
                return vocabulary[word].genitive.singular;
            } else {
                return vocabulary[word][caseName].plural;
            }
        }
        // Числительные, начиная от пяти и далее в И.-В. п. управляют формой Р.п. множественного числа существительных,
        // а в остальных падежах – согласуются с существительными во множественном числе.
        if(['nominative', 'genitive', 'dative', 'accusative'].indexOf(caseName) > -1) {
            return vocabulary[word].genitive.plural;
        } else return vocabulary[word][caseName].plural;
    });
});
